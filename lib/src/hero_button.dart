import 'package:flutter/material.dart';
import 'package:hero_button/src/hero_button_child.dart';

enum HeroButtonStyle {
  circle,
  oval,
  continuousRectangle,
  roundedRectangle,
  beveledRectangle,
}

class HeroButton extends StatelessWidget {
  final VoidCallback? onPressed;

  final Widget? child;
  final String? title;
  final HeroButtonStyle buttonStyle;
  final double? radius;
  final Alignment childAlignment;
  final Alignment buttonAlignment;
  final TextAlign textAlign;
  final double? elevation;
  final Color? shadowColor;
  final TextStyle? textStyle;
  final EdgeInsets? padding;
  final double? height;
  final double? width;
  final Size? size;
  final double? borderWidth;
  final bool isFittedChildToButton;

  //colors
  final Color? buttonColor;
  final Color? disableButtonColor;
  final Color? childColor;
  final Color? disableChildColor;
  final Color? borderColor;

  //loading parameters
  final bool isLoading;
  final Color loadingColor;
  final Color? loadingBackgroundColor;
  final double? loadingSize;
  final double loadingStrokeWidth;

  const HeroButton({
    Key? key,
    required this.onPressed,
    this.child,
    this.title,
    this.loadingSize = 30,
    this.shadowColor,
    this.textStyle,
    this.padding,
    this.height,
    this.width,
    this.size,
    this.buttonColor,
    this.disableButtonColor,
    this.childColor,
    this.disableChildColor,
    this.borderColor,
    this.borderWidth,
    this.loadingBackgroundColor,
    this.buttonStyle = HeroButtonStyle.roundedRectangle,
    this.radius,
    this.loadingStrokeWidth = 3,
    this.isLoading = false,
    this.isFittedChildToButton = false,
    this.loadingColor = Colors.white,
    this.childAlignment = Alignment.center,
    this.buttonAlignment = Alignment.center,
    this.textAlign = TextAlign.center,
    this.elevation,
  })
      : assert(title == null || child == null, 'It is not possible to set both title and child'),
        assert(
        borderWidth == null || borderColor != null, 'for set border you must have borderColor'),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: buttonAlignment,
      child: TextButton(
        onPressed: isLoading ? () {} : onPressed,
        style: TextButton.styleFrom(
          enableFeedback: true,
          alignment: childAlignment,
          padding: padding ??
              Theme
                  .of(context)
                  .textButtonTheme
                  .style
                  ?.padding
                  ?.resolve({MaterialState.pressed}) ??
              const EdgeInsets.all(8),
          elevation: onPressed == null
              ? 0
              : elevation ??
              Theme
                  .of(context)
                  .textButtonTheme
                  .style
                  ?.elevation
                  ?.resolve({MaterialState.pressed}),
          textStyle: textStyle ??
              Theme
                  .of(context)
                  .textButtonTheme
                  .style
                  ?.textStyle
                  ?.resolve({MaterialState.pressed}) ??
              Theme
                  .of(context)
                  .textTheme
                  .button,
          fixedSize: size ??
              (height != null || width != null
                  ? Size(width ?? double.infinity, height ?? double.infinity)
                  : Theme
                  .of(context)
                  .textButtonTheme
                  .style
                  ?.fixedSize
                  ?.resolve({MaterialState.pressed})),
          minimumSize: const Size(20, 20),
          shadowColor: shadowColor ??
              Theme
                  .of(context)
                  .textButtonTheme
                  .style
                  ?.shadowColor
                  ?.resolve({MaterialState.pressed}),
          backgroundColor: buttonColor ??
              Theme
                  .of(context)
                  .textButtonTheme
                  .style
                  ?.backgroundColor
                  ?.resolve({MaterialState.pressed}) ??
              Theme
                  .of(context)
                  .primaryColor,
          disabledBackgroundColor: disableButtonColor ??
              Theme
                  .of(context)
                  .textButtonTheme
                  .style
                  ?.backgroundColor
                  ?.resolve({MaterialState.disabled}) ??
              Theme
                  .of(context)
                  .disabledColor,
          foregroundColor: childColor ??
              Theme
                  .of(context)
                  .textButtonTheme
                  .style
                  ?.foregroundColor
                  ?.resolve({MaterialState.pressed}) ??
              Colors.white,
          disabledForegroundColor: disableChildColor ??
              Theme
                  .of(context)
                  .textButtonTheme
                  .style
                  ?.foregroundColor
                  ?.resolve({MaterialState.disabled}),
          shape: _getButtonStyle(),
          side: borderColor != null
              ? BorderSide(color: borderColor!, width: borderWidth ?? 1.0)
              : null,
        ),
        child: HeroButtonChild(
          title: title,
          textAlign: textAlign,
          isLoading: isLoading,
          loadingColor: loadingColor,
          loadingBackgroundColor: loadingBackgroundColor,
          loadingSize: loadingSize,
          loadingStrokeWidth: loadingStrokeWidth,
          isFittedChildToButton: isFittedChildToButton,
          child: child,
        ),
      ),
    );
  }

  OutlinedBorder? _getButtonStyle() {
    final radius = _getDefaultButtonRadius();
    switch (buttonStyle) {
      case HeroButtonStyle.circle:
        return const CircleBorder();
      case HeroButtonStyle.oval:
        return const StadiumBorder();
      case HeroButtonStyle.roundedRectangle:
        return RoundedRectangleBorder(borderRadius: BorderRadius.circular(radius));
      case HeroButtonStyle.continuousRectangle:
        return ContinuousRectangleBorder(borderRadius: BorderRadius.circular(radius));
      case HeroButtonStyle.beveledRectangle:
        return BeveledRectangleBorder(borderRadius: BorderRadius.circular(radius));
      default:
        return null;
    }
  }

  double _getDefaultButtonRadius() {
    switch (buttonStyle) {
      case HeroButtonStyle.roundedRectangle:
        return radius ?? 10;
      case HeroButtonStyle.continuousRectangle:
        return radius ?? 32;
      case HeroButtonStyle.beveledRectangle:
        return radius ?? 10;
      default:
        return 0;
    }
  }
}
